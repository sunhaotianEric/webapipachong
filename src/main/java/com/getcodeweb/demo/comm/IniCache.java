package com.getcodeweb.demo.comm;

import com.getcodeweb.demo.TaskJob.AiCaiRule;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import com.getcodeweb.demo.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class IniCache {
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    FlushOpenCodeService flushOpenCodeService;

    private static Logger logger = LoggerFactory.getLogger(IniCache.class);
    public void iniDataForRedis(){

        for (String code: AiCaiRule.urlByLotteryCode.keySet()){
            logger.info("正在将彩种：{}的最后10期开奖号码放入redis缓存 中。。。。。",code);
            redisUtil.set(code,flushOpenCodeService.getNewOpenCodebyCodeLimit10(code));
        }
    }
}
