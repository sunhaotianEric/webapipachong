package com.getcodeweb.demo.comm;

import com.alibaba.fastjson.JSONObject;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.util.TrustSSL;
import lombok.Data;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
@Data
public class ChooseProxy {
    private String canUseProxy;

    private static Logger logger = LoggerFactory.getLogger(ChooseProxy.class);

    @SneakyThrows
    public String canUseProxy() throws Exception{

        URL console = new URL("http://167.179.75.153:5010/get_all/");
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) console.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FlushOpenCodeEntity flushOpenCodeEntity = new FlushOpenCodeEntity();
        if (conn instanceof HttpsURLConnection)  {
            SSLContext sc = null;
            try {
                sc = SSLContext.getInstance("SSL");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                sc.init(null, new TrustManager[]{new TrustSSL.TrustAnyTrustManager()}, new java.security.SecureRandom());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
            ((HttpsURLConnection) conn).setHostnameVerifier(new TrustSSL.TrustAnyHostnameVerifier());
            ((HttpsURLConnection) conn).setReadTimeout(999999);
        }
        conn.connect();
        //System.out.println(conn);
        InputStream inputStream = conn.getInputStream();
        //获取响应
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder stringBuilder =new StringBuilder();
        while ((line = reader.readLine()) != null){
            stringBuilder.append(line);
            //System.out.println(line);
        }
        String proxy="";
        Document document;
        List<JSONObject> jsonObject = JSONObject.parseArray(stringBuilder.toString(),JSONObject.class);
        for (JSONObject jsonObject1:jsonObject){
            JSONObject jsonObject2 = JSONObject.parseObject(jsonObject1.toJSONString());
            String []proxys=jsonObject2.get("proxy").toString().split(":");
            try {
                logger.info("正在尝试使用ip：{}连接目标站",proxys[0]+" "+proxys[1]);
                document = Jsoup.connect("https://pub.icaile.com/sd11x5/").proxy(proxys[0], Integer.parseInt(proxys[1])).timeout(10000).get();
                try{
                    if(document.getElementById("firstTbody")!=null){
                        proxy=jsonObject2.get("proxy").toString();
                        break;
                    }
                }catch (Exception e2){
                    logger.info("连接成功可以是参数不对");
                }
            }catch (Exception e){
                logger.info("连接失败");
            }
        }
        reader.close();
        //该干的都干完了,记得把连接断了
        conn.disconnect();
        return proxy;
    }

    public static void main(String[] args) throws Exception {
        ChooseProxy chooseProxy = new ChooseProxy();
        String canUseProxy = chooseProxy.canUseProxy();
        System.out.println(canUseProxy);
    }
}
