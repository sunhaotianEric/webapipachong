package com.getcodeweb.demo.comm;

import com.getcodeweb.demo.service.FlushOpenCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
@Component
public class Test implements ApplicationRunner {
    private static Logger logger = LoggerFactory.getLogger(Test.class);
    @Autowired
    FlushOpenCodeService flushOpenCodeService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
    }
}
