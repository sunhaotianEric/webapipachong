package com.getcodeweb.demo.entity;

import com.getcodeweb.demo.util.XMLUtil;
import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;


@XmlRootElement(name = "item")
public class LotteryResultForKcwVo {
    String expect;

    String opencode;

    String opentime;
    @XmlAttribute(name = "expect")
    public String getExpect() {
        return expect;
    }

    @XmlAttribute(name = "opentime")
    public String getOpentime() {
        return opentime;
    }

    @XmlAttribute(name = "opencode")
    public String getOpencode() {
        return opencode;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }

    public void setOpencode(String opencode) {
        this.opencode = opencode;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }


    public static void main(String[] args) {

        LotteryResultForKcwVo item1 = new LotteryResultForKcwVo();
        item1.setExpect("2018034");
        item1.setOpencode("41,49,31,46,21,29,36");
        item1.setOpentime("2018-04-03 21:35:08");

        LotteryResultForKcwVo item2 = new LotteryResultForKcwVo();
        item2.setExpect("2018033");
        item2.setOpencode("16,13,19,18,09,34,21");
        item2.setOpentime("2018-03-31 21:35:31");

        LotteryResultForKcw resultRoot = new LotteryResultForKcw();
        resultRoot.setXmls(new ArrayList<LotteryResultForKcwVo>());
        resultRoot.getXmls().add(item1);
        resultRoot.getXmls().add(item2);

        String result = XMLUtil.transformToXml(LotteryResultForKcw.class, resultRoot);
        System.out.println(result);
    }
}
