package com.getcodeweb.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Data
@TableName("open_code")

public class FlushOpenCodeEntity implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id ;
    private String lottryNum;
    private String lottryCode;
    private Date createdTime;
    private String code;
    private Date openCodeTime;
    private String webRealLottryNum;

}
