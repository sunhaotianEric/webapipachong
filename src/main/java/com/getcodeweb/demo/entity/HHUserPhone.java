package com.getcodeweb.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("hh_plan_user")
public class HHUserPhone {
    @TableId(type = IdType.AUTO)
    private Integer id ;

    String phoneNUm;
}
