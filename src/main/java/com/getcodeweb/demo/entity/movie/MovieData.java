package com.getcodeweb.demo.entity.movie;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@TableName("movie_data")
public class MovieData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    private String title;

    private String playAddress;

    private String image;

    private Date updateDate;

    private String timeLong;
}
