package com.getcodeweb.demo.entity;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * 开彩网返回结果类
 * @author chenhsh
 *
 */
@XmlRootElement(name = "xml") 
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class LotteryResultForKcw {
	
	@XmlElement(name="row")
	private List<LotteryResultForKcwVo> xmls;

}
