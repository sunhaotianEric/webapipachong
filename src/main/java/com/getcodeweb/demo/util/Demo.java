package com.getcodeweb.demo.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Demo {
    public static void main(String[] args) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date parse = simpleDateFormat.parse("2020-05-07 21:14:01");
        Date parse1 = simpleDateFormat.parse("2020-05-07 21:07:19");
        System.out.println(parse.getTime()-parse1.getTime());
    }
}
