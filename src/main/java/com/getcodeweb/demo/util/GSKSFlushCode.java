package com.getcodeweb.demo.util;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@EnableScheduling
public class GSKSFlushCode {
    private static Logger logger = LoggerFactory.getLogger(GSKSFlushCode.class);

    @Autowired
    FlushOpenCodeService flushOpenCodeService;

    @Autowired
    RedisUtil redisUtil;

    public static Map<String,FlushOpenCodeEntity> map= new HashMap<>();
    //甘肃快3 开奖结果
    //@Scheduled(cron = "0/3 * * * * ?")
    public void sd11x5Code() throws ParseException {
        Document document ;
        try {
            document = Jsoup.parse(new URL("https://kjh.55128.cn/history_gansuk3.aspx"), 999999999);

        }catch (Exception e){
            logger.info("连接失败，尝试重连");
            return;
        }
        Elements elements = null;
        List<FlushOpenCodeEntity> flushOpenCodeEntities =new ArrayList<>();
        elements=document.getElementsByTag("tr");
        List<String>hasCache=new ArrayList<>();
        for (int i=0;i<elements.size();i++){
            if(i==0)continue;
            Elements elementsByClass = elements.get(i).getElementsByClass("td-number");
            String codeNum=elementsByClass.get(2).getElementsByClass("ball-list red").text().replace(" ",",");
            FlushOpenCodeEntity flush7xcEntity = new FlushOpenCodeEntity();
            flush7xcEntity.setCode("GSKS");
            flush7xcEntity.setCreatedTime(new Date());
            flush7xcEntity.setLottryNum(elementsByClass.get(1).text());
            flush7xcEntity.setLottryCode(codeNum);
            SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd hh:mm");
            flush7xcEntity.setOpenCodeTime(simpleDateFormat.parse(elementsByClass.get(0).text()));
            flushOpenCodeEntities.add(flush7xcEntity);
            String key = elementsByClass.get(1).text()+flush7xcEntity.getCode();
            FlushOpenCodeEntity result = map.get(key);

            if(result==null) {
                logger.info("缓存中没有甘肃快3"+flush7xcEntity.getLottryNum()+"的数据，到数据库查询");

                result = flushOpenCodeService.getOne(new QueryWrapper<FlushOpenCodeEntity>()
                        .eq("lottry_num", flush7xcEntity.getLottryNum())
                        .eq("code", "GSKS"));
                map.put(key,result);
                if (result == null) {
                    logger.info("没有" + "GSKS" + "的对应数据，正在更新数据");
                    if (flushOpenCodeService.save(flush7xcEntity)) {
                        map.put(key,flush7xcEntity);
                        logger.info("更新" + "GSKS" + "数据完毕");
                        logger.info("更新" + "GSKS" + "数据,并且将最新开奖结果期号对象装入缓存完毕");
                        logger.info("目前map中的缓存"+map);
                    } else {
                        logger.info("更新" + "GSKS" + "数据失败");
                    }
                }
            }else {
                hasCache.add(result.getLottryNum());
            }
        }
        logger.info("甘肃快3第"+hasCache+"期缓存有数据，无需查数据库");

    }


}
