package com.getcodeweb.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.getcodeweb.demo.controller.GetYellowData;
import com.getcodeweb.demo.entity.movie.MovieData;
import org.springframework.stereotype.Service;

@Service
public interface GetYellowDataService extends IService<MovieData> {
}
