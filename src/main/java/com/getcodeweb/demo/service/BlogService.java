package com.getcodeweb.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.getcodeweb.demo.entity.blog.Blog;
import org.springframework.stereotype.Service;

@Service
public interface BlogService extends IService<Blog> {
}
