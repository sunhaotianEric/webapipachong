package com.getcodeweb.demo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FlushOpenCodeService extends IService<FlushOpenCodeEntity> {
    List<FlushOpenCodeEntity> getNewOpenCodebyCodeLimit10(String code);

    Integer cheakAndUpdateVoid(String lottryNum,String code);
}
