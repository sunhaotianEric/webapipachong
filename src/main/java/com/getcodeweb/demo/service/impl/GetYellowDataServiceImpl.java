package com.getcodeweb.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.getcodeweb.demo.controller.GetYellowData;
import com.getcodeweb.demo.entity.movie.MovieData;
import com.getcodeweb.demo.mapper.movie.GetYellowDataMapper;
import com.getcodeweb.demo.service.GetYellowDataService;
import org.springframework.stereotype.Service;

@Service
public class GetYellowDataServiceImpl extends ServiceImpl<GetYellowDataMapper, MovieData> implements GetYellowDataService {
}
