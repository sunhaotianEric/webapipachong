package com.getcodeweb.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.mapper.FlushOpenCodeMapper;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FlushOpenCodeServiceImpl extends ServiceImpl<FlushOpenCodeMapper, FlushOpenCodeEntity> implements FlushOpenCodeService {

    @Autowired
    FlushOpenCodeMapper flushOpenCodeMapper;
    @Override
    public List<FlushOpenCodeEntity> getNewOpenCodebyCodeLimit10(String code) {
        return flushOpenCodeMapper.getNewOpenCodebyCodeLimit10(code);
    }

    @Override
    @Transactional(rollbackFor = ArithmeticException.class)
    public Integer cheakAndUpdateVoid(String lottryNum, String code) {
        FlushOpenCodeEntity flushOpenCodeEntity = flushOpenCodeMapper.selectOne(new QueryWrapper<FlushOpenCodeEntity>().eq("lottry_num", lottryNum)
                .eq("code", code));

        flushOpenCodeEntity.setLottryCode("66666");

        int update = flushOpenCodeMapper.update(flushOpenCodeEntity, new QueryWrapper<FlushOpenCodeEntity>().eq("lottry_num", lottryNum)
                .eq("code", code));
        System.out.println("成功更新："+update);
        int i =10/0;

        return 1;
    }
}
