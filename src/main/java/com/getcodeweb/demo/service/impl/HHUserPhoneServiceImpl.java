package com.getcodeweb.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.getcodeweb.demo.entity.HHUserPhone;
import com.getcodeweb.demo.mapper.HHUserPhoneMapper;
import com.getcodeweb.demo.service.HHUserPhoneService;
import org.springframework.stereotype.Service;

@Service
public class HHUserPhoneServiceImpl extends ServiceImpl<HHUserPhoneMapper, HHUserPhone> implements HHUserPhoneService {
}
