package com.getcodeweb.demo.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.getcodeweb.demo.entity.blog.Blog;
import com.getcodeweb.demo.mapper.myself.BlogMapper;
import com.getcodeweb.demo.service.BlogService;
import org.springframework.stereotype.Service;

@Service
public class BlogImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {
}
