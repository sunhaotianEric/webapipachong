package com.getcodeweb.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.getcodeweb.demo.entity.HHUserPhone;
import org.springframework.stereotype.Service;

@Service
public interface HHUserPhoneService  extends IService<HHUserPhone> {
}
