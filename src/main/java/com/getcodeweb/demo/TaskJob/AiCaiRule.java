package com.getcodeweb.demo.TaskJob;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import com.getcodeweb.demo.util.HttpClientUtil;
import com.getcodeweb.demo.util.RedisUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@EnableScheduling
public class AiCaiRule {
    private static Logger logger = LoggerFactory.getLogger(AiCaiRule.class);

    private static String aiCaiUrl = "https://kclm.xyz/graphql";

    private static String lock = "lock";
    @Autowired
    FlushOpenCodeService flushOpenCodeService;

    @Autowired
    RedisUtil redisUtil;

    public static  HashMap<String,String> urlByLotteryCode = new HashMap<>();

    private static ExecutorService threadPool = Executors.newFixedThreadPool(10);


    static {
        urlByLotteryCode.put("XJSSC","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"xjssc\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("CQSSC","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"cqssc\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("TJSSC","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"tjssc\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("SH11X5","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"sh11x5\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("JX11X5","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"jx11x5\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("GD11X5","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"gd11x5\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("SD11X5","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"sd11x5\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("JSSYXW","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"jssyxw\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("BTCFFC","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"btcffc\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("BJK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"bjk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("SHK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"shk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("JSK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"jsk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("GXKS","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"gxks\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("HUBK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"hubk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("JLK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"jlk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("GSK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"gsk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("AHK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"ahk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("HUBK3","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"hubk3\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");

        urlByLotteryCode.put("FC3D","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"fc3d\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("XYFT","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"xyft\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        urlByLotteryCode.put("BJPK10","{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"bjpk10\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");

    }


    public static HashMap<String, FlushOpenCodeEntity> cache = new HashMap<>();

    public static void main(String[] args) {
        String a = "2022121327";
        String substring = a.substring(0, 8);
        a=substring+"0"+a.substring(8);
        System.out.println(a);
        /*JSONObject jsonObject = HttpClientUtil.httpPostSms(aiCaiUrl, "{\"operationName\":\"drawHistory\",\"variables\":{},\"query\":\"query drawHistory {\\n  info: drawHistory(code: \\\"sd11x5\\\") {\\n    issue\\n    drawResult\\n    drawTime\\n    __typename\\n  }\\n}\\n\"}");
        JSONArray data = jsonObject.getJSONObject("data").getJSONArray("info");
        for (Object datum : data) {
            System.out.println(">>>>>"+datum);
           // System.out.println(JSONObject.parseObject(datum.toString()).get("issue"));
        }*/

    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Scheduled(cron = "0/10 * * * * ?")
    public void cqssc() throws IOException, ParseException {
        for(String key:urlByLotteryCode.keySet()){
            Thread thread = new Thread() {
                @Override
                public void run() {
                    synchronized (lock) {
                        JSONObject jsonObject = HttpClientUtil.httpPostSms(aiCaiUrl, urlByLotteryCode.get(key));
                        if (jsonObject == null) return;
                        JSONArray data = jsonObject.getJSONObject("data").getJSONArray("info");
                        FlushOpenCodeEntity flushOpenCodeEntity = new FlushOpenCodeEntity();
                        for (Object datum : data) {
                            jsonObject = JSONObject.parseObject(datum.toString());
                            String lottryNum = jsonObject.get("issue").toString();
                            if (key.equals("XJSSC")) {
                                //新疆时时彩期号少了个0，特殊处理一下
                                String substring = lottryNum.substring(0, 8);
                                lottryNum = substring + "0" + lottryNum.substring(8);
                            }
                            String openTime = jsonObject.get("drawTime").toString();
                            String lottryCode = jsonObject.get("drawResult").toString();
                            flushOpenCodeEntity.setLottryNum(lottryNum);
                            flushOpenCodeEntity.setLottryCode(lottryCode);
                            flushOpenCodeEntity.setWebRealLottryNum(lottryNum);
                            flushOpenCodeEntity.setCode(key);
                            flushOpenCodeEntity.setCreatedTime(new Date());
                            try {
                                flushOpenCodeEntity.setOpenCodeTime(simpleDateFormat.parse(openTime));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String key = flushOpenCodeEntity.getCode() + lottryNum;
                            FlushOpenCodeEntity result = cache.get(key);
                            if (result == null) {
                                logger.info("缓存中没有" + flushOpenCodeEntity.getCode() + "第" + flushOpenCodeEntity.getLottryNum() + "期的数据，到数据库查询");
                                result = flushOpenCodeService.getOne(new QueryWrapper<FlushOpenCodeEntity>().
                                        eq("code", flushOpenCodeEntity.getCode()).
                                        eq("web_real_lottry_num", flushOpenCodeEntity.getWebRealLottryNum()));
                                cache.put(key, result);
                                if (result == null) {
                                    cache.put(key, result);
                                    logger.info("没有彩种：" + flushOpenCodeEntity.getCode() + " 第" + flushOpenCodeEntity.getLottryNum() + "期" + "的对应数据，正在更新数据");
                                    if (flushOpenCodeService.save(flushOpenCodeEntity)) {
                                        cache.put(key, flushOpenCodeEntity);
                                        //保存成功之后查询数据库对应彩种最近10期的开奖号码信息
                                        //保存到redis中
                                        logger.info("正在更新彩种{}最后10期的开奖信息到redis中", flushOpenCodeEntity.getCode());
                                        redisUtil.set(flushOpenCodeEntity.getCode(),
                                                flushOpenCodeService.getNewOpenCodebyCodeLimit10(flushOpenCodeEntity.getCode()));
                                        logger.info("更新" + flushOpenCodeEntity.getCode() + "数据,并且将最新开奖结果期号对象同步到redis缓存完毕");
                                    } else {
                                        logger.info("更新" + flushOpenCodeEntity.getCode() + "数据失败");
                                    }
                                }
                            }
                        }
                    }
                }
            };
            //logger.info("启动了线程："+thread.currentThread().getName());
            threadPool.execute(thread);
        }
    }


    @Scheduled(cron = "0 0 0 * * ?")
    public void clearMap() {
        logger.info("目前map缓存：" + cache);
        cache.clear();
        logger.info("清除之后：" + cache);
    }
}
