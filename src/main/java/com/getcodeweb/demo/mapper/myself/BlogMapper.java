package com.getcodeweb.demo.mapper.myself;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.getcodeweb.demo.entity.blog.Blog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlogMapper extends BaseMapper<Blog> {
}
