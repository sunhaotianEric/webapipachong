package com.getcodeweb.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FlushOpenCodeMapper extends BaseMapper<FlushOpenCodeEntity> {
    @Select("select * from open_code  where code=#{code} order  by created_time desc limit 0,10")
    List<FlushOpenCodeEntity> getNewOpenCodebyCodeLimit10(String code);

}
