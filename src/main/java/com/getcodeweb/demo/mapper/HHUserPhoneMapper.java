package com.getcodeweb.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.getcodeweb.demo.entity.HHUserPhone;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HHUserPhoneMapper extends BaseMapper<HHUserPhone> {
}
