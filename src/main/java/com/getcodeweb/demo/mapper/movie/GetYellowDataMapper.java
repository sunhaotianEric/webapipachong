package com.getcodeweb.demo.mapper.movie;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.getcodeweb.demo.controller.GetYellowData;
import com.getcodeweb.demo.entity.movie.MovieData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GetYellowDataMapper extends BaseMapper<MovieData> {
}
