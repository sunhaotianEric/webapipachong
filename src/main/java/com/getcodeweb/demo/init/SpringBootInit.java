package com.getcodeweb.demo.init;

import com.getcodeweb.demo.comm.IniCache;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import com.getcodeweb.demo.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
@Component
public class SpringBootInit{
    @Autowired
    IniCache iniCache;
    @PostConstruct
    public void iniDataForRedis(){
        iniCache.iniDataForRedis();
    }

}
