package com.getcodeweb.demo.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
    /**
     * @author eric
     * @title: MybatisPlusConfig
     * @projectName ybadmin
     * @description: TODO
     * @date 2019/5/2415:02
     */
    @EnableTransactionManagement
    @Configuration
    @MapperScan({"com.getcodeweb.demo.mapper"})
    public class MybatisPlusConfig {


        @Bean
        public PaginationInterceptor paginationInterceptor() {
            return new PaginationInterceptor();
        }

        //mybatis-plus乐观锁配置
        @Bean
        public OptimisticLockerInterceptor optimisticLockerInterceptor() {
            return new OptimisticLockerInterceptor();
        }

    }
