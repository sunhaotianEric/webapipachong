package com.getcodeweb.demo.controller;

import com.getcodeweb.demo.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SwitchProxyController {
    @Autowired
    RedisUtil redisUtil;
    @GetMapping("/switchProxy")
    public String switchProxy(@RequestParam String address){
        redisUtil.set("PROXY",address);
        return "ok";
    }
    @GetMapping("/calibrationBjksnum")
    public String setbjksnum(@RequestParam String num){
        redisUtil.set("bjksNum",num);
        return "ok";
    }
}
