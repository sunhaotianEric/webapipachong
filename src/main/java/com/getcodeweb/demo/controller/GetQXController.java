package com.getcodeweb.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URL;
import java.util.*;

@RestController
@RequestMapping("/")
public class GetQXController {
    @Autowired
    FlushOpenCodeService flush7xcService;
    @RequestMapping("get7xcCode")
    public String get7xcCode(@RequestParam String code ) throws Exception {
        return flush7xcService.list(new QueryWrapper<FlushOpenCodeEntity>().eq("code", code)
                .orderByDesc("created_time")).get(0).toString();
    }

    @RequestMapping("update7xcHistory")
    public String update7xcHistory() throws Exception{
        Set<FlushOpenCodeEntity> entities = new HashSet<>();
        for (int i = 0; i < 121; i++) {
            Document document = Jsoup.parse(new URL("http://www.lottery.gov.cn/historykj/history_"+i+".jspx?_ltype=qxc"), 999999999);
            Elements elements = document.getElementsByClass("result")
                    .select("tbody")
                    .select("tr");
            Map<String, String> map = new HashMap<>();
            for (Element element : elements) {
                map.put(element.select("td").get(0).text(), element.select("td").get(1).text());
            }
            map.remove("20012");
            for (String key : map.keySet()) {
                System.err.println("key:"+key+" value:"+map.get(key));
                String lottryNum= key;
                String lottryCode= map.get(key);
                FlushOpenCodeEntity flush7xcEntity = new FlushOpenCodeEntity();
                flush7xcEntity.setLottryCode(lottryCode);
                flush7xcEntity.setLottryNum(lottryNum);
                flush7xcEntity.setCode("07XC");
                flush7xcEntity.setCreatedTime(new Date());
                entities.add(flush7xcEntity);
            }
            System.err.println("正在采集第"+i+"页信息。。。");
            System.err.println("目前一共"+entities.size()+"条数据");
        }
        boolean b = flush7xcService.saveBatch(entities);
        if(b)return "更新七星彩历史数据完成";
        return "更新七星彩历史数据失败";
    }
}
