package com.getcodeweb.demo.controller.privateBlog;

import com.getcodeweb.demo.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class BlogController {
    @Autowired
    RedisUtil redisUtil;

    @RequestMapping("/privateBlog")
    public String privateBlog(){
        return "index.html";
    }

    @RequestMapping("/getIsOpen")
    public String getIsOpen(@RequestParam("code") String code, HttpServletRequest request){
        System.out.println(request.getRemoteAddr()+"请求了");
        return "ok";
    }
}
