package com.getcodeweb.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.getcodeweb.demo.entity.movie.MovieData;
import com.getcodeweb.demo.service.GetYellowDataService;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RestController
public class GetYellowData {
//x-forwarded-for 可 通过这个伪造 ip
    @Autowired
    GetYellowDataService getYellowDataService;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @RequestMapping("saveData")
    public  String saveData() throws IOException, InterruptedException {
        new Thread(){
            @Override
            public void run() {
                for(int i =108;i<150;i++){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("线程"+this.getName()+"：第" + i + "页");

                    Set<String> urls =new HashSet<>();
                    Map<String,String >requestHeard = new HashMap<>();
                    requestHeard.put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    requestHeard.put("Accept-Encoding","gzip, deflate");
                    requestHeard.put("Accept-Language","zh-CN,zh;q=0.9,en;q=0.8");
                    requestHeard.put("Cache-Control","max-age=0");
                    requestHeard.put("Connection","keep-alive");
                    requestHeard.put("Cookie","CLIPSHARE=5st1s3bvqls76fuk41soa6d312; __utma=12711935.1773655380.1586566815.1586566815.1586566815.1; __utmb=12711935.0.10.1586566815; __utmc=12711935; __utmz=12711935.1586566815.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __dtsu=10401586566817D05984C690BBA384AF; watch_times=3");
                    requestHeard.put("Host","999.mh8z.cn");
                    requestHeard.put("Referer","http://999.mh8z.cn/index.php?reg=1");
                    requestHeard.put("Upgrade-Insecure-Requests","1");
                    requestHeard.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36");
                    Element element=null;
                    boolean fail = true;
                    String referer ="";
                    while (fail){
                        try {
                            referer="http://999.mh8z.cn/v.php?category=long&viewtype=basic&page="+i;
                            element =Jsoup.connect(referer)
                                    .timeout(10000)
                                    .headers(requestHeard)
                                    .get();

                            fail=false;
                        }catch (Exception e){
                            System.out.println("线程"+this.getName()+"：连接失败，尝试重连");
                        }
                    }


                    Elements elements =  element.getElementsByClass("well well-sm videos-text-align");
                    Elements div = elements.select("div");
                    String clean = Jsoup.clean(div.html(), Whitelist.basic());
                    Document parse = Jsoup.parse(clean);
                    Elements a = parse.select("a");
                    Elements elements1 = a.prevAll();
                    String b = elements1.toString();
                    String[] split = b.split("<a href=\"");
                    for(String s:split){
                        if(s.startsWith("http://")){
                            urls.add(s.split("\"")[0]);
                        }
                    }
                    try {
                        openUrlsListGetPlayAddress(urls,referer);break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                for(int i =173;i>150;i--){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("线程"+this.getName()+"：第" + i + "页");

                    Set<String> urls =new HashSet<>();
                    Map<String,String >requestHeard = new HashMap<>();
                    requestHeard.put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    requestHeard.put("Accept-Encoding","gzip, deflate");
                    requestHeard.put("Accept-Language","zh-CN,zh;q=0.9,en;q=0.8");
                    requestHeard.put("Cache-Control","max-age=0");
                    requestHeard.put("Connection","keep-alive");
                    requestHeard.put("Cookie","CLIPSHARE=5st1s3bvqls76fuk41soa6d312; __utma=12711935.1773655380.1586566815.1586566815.1586566815.1; __utmb=12711935.0.10.1586566815; __utmc=12711935; __utmz=12711935.1586566815.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __dtsu=10401586566817D05984C690BBA384AF; watch_times=3");
                    requestHeard.put("Host","999.mh8z.cn");
                    requestHeard.put("Referer","http://999.mh8z.cn/index.php?reg=1");
                    requestHeard.put("Upgrade-Insecure-Requests","1");
                    requestHeard.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36");
                    Element element=null;
                    boolean fail = true;
                    String referer ="";
                    while (fail){
                        try {
                            referer="http://999.mh8z.cn/v.php?category=long&viewtype=basic&page="+i;
                            element =Jsoup.connect(referer)
                                    .timeout(10000)
                                    .headers(requestHeard)
                                    .get();

                            fail=false;
                        }catch (Exception e){
                            System.out.println("线程"+this.getName()+"连接失败，尝试重连");
                        }
                    }


                    Elements elements =  element.getElementsByClass("well well-sm videos-text-align");
                    Elements div = elements.select("div");
                    String clean = Jsoup.clean(div.html(), Whitelist.basic());
                    Document parse = Jsoup.parse(clean);
                    Elements a = parse.select("a");
                    Elements elements1 = a.prevAll();
                    String b = elements1.toString();
                    String[] split = b.split("<a href=\"");
                    for(String s:split){
                        if(s.startsWith("http://")){
                            urls.add(s.split("\"")[0]);
                        }
                    }
                    try {
                        openUrlsListGetPlayAddress(urls,referer);
                    } catch (IOException e) {
                        e.printStackTrace();break;

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();


        return "程序开始执行";
    }



    public synchronized void openUrlsListGetPlayAddress(Set<String> urls,String referer) throws IOException, InterruptedException {
        System.out.println("数据："+urls.size());
        for(String url:urls){

            Thread.sleep(1000);
            Map<String,String> headers = new HashMap<>();
            headers.put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            headers.put("Accept-Encoding","gzip, deflate");
            headers.put("Accept-Language","zh-CN,zh;q=0.9,en;q=0.8");
            headers.put("Cache-Control","max-age=0");
            headers.put("Connection","keep-alive");
            headers.put("Cookie","__dtsu=6BB6E72D5D662701355E205E2A9B4102; CLIPSHARE=rv97mfj7n13mjv6fpmcd2n4tk1; __utma=12711935.1304392290.1586568838.1586568838.1586568838.1; __utmb=12711935.0.10.1586568838; __utmc=12711935; __utmz=12711935.1586568838.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); 91username=d74edpL8T4UT4bu6jY6vrJ3Vzqs8%2FLgJFtWmEAh8w9QKfglWhs2DEvHt6A; DUID=69fdmtmnIH0Ej1upcnr1ecKsdkZQFvZM35cYXIWOa%2F6bVEOH; USERNAME=28b1AX0iPAoU9wQDKbWP7VsNn%2BM9bG7w%2B8AmjZ6aEm3jlHonUkJ58tNiqA; EMAILVERIFIED=no; watch_times=4");
            headers.put("Host","999.mh8z.cn");
            headers.put("Referer",referer);
            headers.put("Upgrade-Insecure-Requests","1");
            headers.put("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50");
            System.out.println("正在解析网址："+url);
            Document document =null;
            boolean fail = true;
            while (fail){
                try {
                    document = Jsoup.connect(url)
                            .timeout(10000)
                            .headers(headers)
                            .get();
                    fail=false;
                }catch (Exception e){
                    System.out.println("连接失败，尝试重连");
                }
            }

            MovieData movieData = new MovieData();
            try {
                String title=document
                        .title()
                        .split("Chinese")[0];
                String image = document.body()
                        .getElementById("player_one")
                        .toString()
                        .split("poster=\"")[1].split("\"")[0];
                String playAddress =document.body()
                        .getElementById("player_one")
                        .html()
                        .split("\n")[0]
                        .split("\"")[1]
                        .split("\\?")[0];
                System.out.println("源网址："+playAddress);
                if(playAddress.contains("http://198.255.82.91")){
                    System.out.println("包含http://198.255.82.91 地址，替换成：http://s.mh8z.cn/");
                    playAddress =playAddress.split("//")[2];
                    playAddress="http://s.mh8z.cn//"+playAddress;
                    System.out.println("替换之后："+playAddress);
                }else if(!playAddress.contains("\\?")&&!playAddress.contains("http://23.225.233.3")){
                    String [] a =playAddress.split("/");
                    playAddress="http://s.mh8z.cn//"+a[3]+"/"+a[4];
                    System.out.println("没有问号，替换成："+playAddress);
                }
                String timeLong = document.body()
                        .getElementsByClass("video-info-span")
                        .get(0).text();

                MovieData data = getYellowDataService.getOne(new QueryWrapper<MovieData>()
                        .eq("title", title)
                        .eq("image",image)
                        .eq("play_address",playAddress));
                if(data!=null){
                    System.out.println("该资源已存在，跳过");
                    continue;
                }
                movieData.setImage(image);
                movieData.setPlayAddress(playAddress);
                movieData.setTitle(title);
                movieData.setTimeLong(timeLong);
                movieData.setUpdateDate(simpleDateFormat.parse(document.body().getElementsByClass("title-yakov").get(1).text()));
                boolean save = getYellowDataService.save(movieData);
                if(save){
                    System.out.println("已保存图片"+document.body().getElementById("player_one").toString().split("poster=\"")[1].split("\"")[0]);
                    System.out.println("已保存标题："+movieData.getTitle());
                    System.out.println("已保存播放地址："+movieData.getPlayAddress());
                    System.out.println("视频上传时间："+movieData.getUpdateDate());
                    System.out.println("视频播放时长："+timeLong);
                }
            }catch (Exception e){
                System.out.println("出现空指针异常");
                WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null,"QQ");
                boolean showed = User32.INSTANCE.ShowWindow(hwnd, WinUser.SW_RESTORE );
                System.out.println("QQ"+(showed?"窗口之前可见.":"窗口之前不可见."));
                    e.printStackTrace();
            }

        }
    }
}
