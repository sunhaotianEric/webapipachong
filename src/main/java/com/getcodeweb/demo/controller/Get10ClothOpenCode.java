package com.getcodeweb.demo.controller;

import com.getcodeweb.demo.comm.IniCache;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.entity.LotteryResultForKcw;
import com.getcodeweb.demo.entity.LotteryResultForKcwVo;
import com.getcodeweb.demo.init.SpringBootInit;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import com.getcodeweb.demo.util.RedisUtil;
import com.getcodeweb.demo.util.XMLUtil;
import com.sun.org.apache.bcel.internal.generic.I2F;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@RestController
public class Get10ClothOpenCode {
    @Autowired
    FlushOpenCodeService flushOpenCodeService;
    @Autowired
    RedisUtil redisUtil;

    private static Logger logger = LoggerFactory.getLogger(Get10ClothOpenCode.class);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @RequestMapping(value = "/getNewOpenCodebyCodeLimit10",method = RequestMethod.GET,produces = MediaType.APPLICATION_XML_VALUE)
    public Object getNewOpenCodebyCodeLimit10(@RequestParam String code, HttpServletRequest request ) throws Exception {
        List<FlushOpenCodeEntity> newOpenCodebyCodeLimit10 = new ArrayList<>();
        try {
            newOpenCodebyCodeLimit10 = (List<FlushOpenCodeEntity> )redisUtil.get(code);//flushOpenCodeService.getNewOpenCodebyCodeLimit10(code);
            if (newOpenCodebyCodeLimit10==null||newOpenCodebyCodeLimit10.isEmpty()) return "is not have data1" ;
        }catch (Exception e){
            return "is not have data2" ;
        }
        List<LotteryResultForKcwVo>rows =new ArrayList<>();
        for (FlushOpenCodeEntity entity:newOpenCodebyCodeLimit10){
            LotteryResultForKcwVo row =new LotteryResultForKcwVo();
            row.setExpect(entity.getLottryNum());
            row.setOpencode(entity.getLottryCode());
            row.setOpentime(simpleDateFormat.format(entity.getCreatedTime()));
            rows.add(row);
        }
        LotteryResultForKcw resultRoot = new LotteryResultForKcw();
        resultRoot.setXmls(rows);
        String xmlResult =XMLUtil.transformToXml(LotteryResultForKcw.class, resultRoot);
        return xmlResult;
    }
    @Autowired
    IniCache iniCache;
    @RequestMapping(value = "/flushCache",method = RequestMethod.GET)
    public String flushCache(){
        logger.info("正在刷新开奖缓存");
        iniCache.iniDataForRedis();
        return "ok";
    }
}
