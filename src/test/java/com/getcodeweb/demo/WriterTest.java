package com.getcodeweb.demo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;

import javax.net.ssl.*;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.List;
import java.util.Timer;

public class WriterTest extends JFrame {
    private static final long serialVersionUID=-9077023825514749548L;
    private static JTextArea ta_showText;    //定义显示文件属性的文本域
    private static JTextArea ta_showProperty;    //定义显示文件内容的文本域

    public void windowClosing(WindowEvent e){
        System.exit(0); //实现以"X"关闭bai窗du口的功zhi能
    }
    //Launch the application.
    public static void main(String[] args) throws Exception {
        WriterTest frame=new WriterTest();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


            File filename = new File("C:\\Users\\ADMIN\\TestCmd\\betProperty.txt");
            File filename1 = new File("C:\\Users\\ADMIN\\TestCmd\\betProperty1.txt");
            File filename2 = new File("C:\\Users\\ADMIN\\TestCmd\\betProperty2.txt");
            List<File> fileList = new ArrayList<>();
            fileList.add(filename);
            fileList.add(filename1);
            fileList.add(filename2);


            for (int i =0;i<fileList.size();i++){
                Timer timer = new Timer();
                File file= fileList.get(i);

                BufferedReader reader= null;
                String tempString = null;
                StringBuilder stringBuilder = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
                Map<String,String > map= new HashMap<>();
                while ((tempString = reader.readLine()) != null) {
                    if (tempString.startsWith("\"投注号码\"")){
                        String[] split = tempString.split(":");
                        map.put("投注号码",split[1]);
                        continue;
                    }
                    stringBuilder.append(tempString);
                }

                JSONObject jsonObject = JSONObject.parseObject(JSON.toJSON(stringBuilder).toString());
                openTextFile("用户id："+jsonObject.get("sessionId")+"配置参数：\n"+JSON.toJSON(stringBuilder).toString());
                reader.close();

                timer.schedule(new TimerTask() {
                    @SneakyThrows
                    @Override
                    public void run() {
                        URL get = new URL("http://localhost:3001/getIsOpen?code=12341");
                        URLConnection urlConnection = get.openConnection();
                        urlConnection.connect();

                        InputStream inputStream1 = urlConnection.getInputStream();
                        //获取响应
                        BufferedReader reader1 = new BufferedReader(new InputStreamReader(inputStream1));
                        String line1;
                        StringBuilder stringBuilder1 =new StringBuilder();
                        while ((line1 = reader1.readLine()) != null){
                            stringBuilder1.append(line1);
                        }
                        if(!"ok".equals(stringBuilder1.toString()))timer.cancel();



                        openTextFile(jsonObject.get("sessionId") + "正在执行,投注彩种："+jsonObject.get("彩种")+"\n，投注号码："+map.get("投注号码")+"期号："+jsonObject.get("开始期号"));
                        int num=Integer.parseInt(jsonObject.getString("开始期号"));

                        PrintWriter out = null;
                        BufferedReader in = null;
                        String result = "";
                        String param = "";
                        Map<String,String> paramMap=new HashMap<>();
                        paramMap.put("command","BET");
                        paramMap.put("sessionId",jsonObject.getString("sessionId"));
                        paramMap.put("oddsAdapt","true");
                        paramMap.put("bets",map.get("投注号码"));
                        paramMap.put("gameType",jsonObject.getString("彩种"));
                        paramMap.put("gameNo",num+"");
                        paramMap.put("timestamps",new Date().getTime()+"");
                        paramMap.put("hasPlayerInfo","true");
                        Iterator<String> it = paramMap.keySet().iterator();
                        while (it.hasNext()) {
                            String key = it.next();
                            param += key + "=" + paramMap.get(key) + "&";
                        }
                        URL console = new URL("https://ssc.yykxg1.com/lotteryweb/WebClientAgent?sessionId="+jsonObject.get("sessionId"));
                        HttpURLConnection conn = (HttpURLConnection) console.openConnection();

                        if (conn instanceof HttpsURLConnection)  {
                            SSLContext sc = SSLContext.getInstance("SSL");
                            sc.init(null, new TrustManager[]{new TrustAnyTrustManager()}, new java.security.SecureRandom());
                            ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
                            ((HttpsURLConnection) conn).setHostnameVerifier(new TrustAnyHostnameVerifier());
                            conn.setReadTimeout(999999);
                            conn.setDoOutput(true);
                            conn.setDoInput(true);
                            conn.setRequestProperty(":authority","ssc.yykxg1.com");
                            conn.setRequestProperty(":method","POST");
                            conn.setRequestProperty("::path","/lotteryweb/WebClientAgent?sessionId="+jsonObject.getString("sessionId"));
                            conn.setRequestProperty(":scheme","https");
                            conn.setRequestProperty("accept","*/*");
                            conn.setRequestProperty("accept-encoding","gzip, deflate, br");
                            conn.setRequestProperty("accept-language","zh-CN,zh;q=0.9,en;q=0.8");
                            conn.setRequestProperty("content-length",jsonObject.getString("content-length"));
                            conn.setRequestProperty("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                            conn.setRequestProperty("cookie",jsonObject.getString("cookie"));
                            conn.setRequestProperty("origin","https://ssc.yykxg1.com");
                            conn.setRequestProperty("referer",jsonObject.getString("referer"));
                            conn.setRequestProperty("sec-fetch-dest","empty");
                            conn.setRequestProperty("sec-fetch-mode","cors");
                            conn.setRequestProperty("sec-fetch-site","same-origin");
                            conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
                            conn.setRequestProperty("x-requested-with","XMLHttpRequest");
                        }
                        out = new PrintWriter(conn.getOutputStream());
                        out.print(param);
                        // flush输出流的缓冲
                        out.flush();
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"ISO-8859-1"));
                        String line;
                        while ((line = in.readLine()) != null) {
                            result += line;
                        }
                        openTextFile(jsonObject.get("sessionId")+"投注成功,\n投注号码:"+map.get("投注号码")+"ok，期号:"+num+",投注时间:"+new Date());
                        num++;
                        jsonObject.put("开始期号",num+"");

                        openTextFile(jsonObject.get("sessionId")+"执行结束，\n下一期投注期号："+jsonObject.get("开始期号"));

                    }
                },0,Long.parseLong(jsonObject.getString("开奖间隔")));
                Thread.sleep(5000);
            }
    }
    public WriterTest()
    {
        setTitle("文本编辑器");    //设置窗体标题
        setBounds(100,100,400,250);    //设置窗体位置和大小
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);    //设置窗体默认关闭方式
        final JMenuBar menuBar=new JMenuBar();    //创建菜单栏
        setJMenuBar(menuBar);    //把菜单栏放到窗体上
        final JMenu mn_file=new JMenu();    //创建文件菜单
        mn_file.setText("文件");    //为文件菜单设置标题
        menuBar.add(mn_file);    //把文件菜单添加到菜单栏上
        final JMenuItem mi_open=new JMenuItem();    //创建打开菜单项
        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) { }
            @Override
            public void windowClosing(WindowEvent e) { }

            @Override
            public void windowClosed(WindowEvent e) { System.exit(0); }
            @Override
            public void windowIconified(WindowEvent e) { }
            @Override
            public void windowDeiconified(WindowEvent e) { }
            @Override
            public void windowActivated(WindowEvent e) { }
            @Override
            public void windowDeactivated(WindowEvent e) { }
        });
        mi_open.setText("打开");    //设置打开菜单项的标题
        mn_file.add(mi_open);    //把打开菜单项添加到文件菜单
        mn_file.addSeparator();    //添加菜单分隔符
        final JMenuItem mi_exit=new JMenuItem();    //创建退出菜单项

        mi_exit.setText("退出");    //设置退出菜单项的标题
        mn_file.add(mi_exit);    //把退出菜单项添加到文件菜单
        final JMenu mn_edit=new JMenu();    //创建编辑菜单
        mn_edit.setText("编辑");    //为编辑菜单设置标题
        menuBar.add(mn_edit);    //把编辑菜单添加到菜单栏上
        final JMenuItem mi_copy=new JMenuItem();    //创建复制菜单项
        mi_copy.setText("复制");    //设置复制菜单项的标题
        mn_edit.add(mi_copy);    //把复制菜单项添加到编辑菜单
        final JMenuItem mi_cut=new JMenuItem();    //创建剪切菜单项
        mi_cut.setText("剪切");    //设置剪切菜单项的标题
        mn_edit.add(mi_cut);    //把剪切菜单项添加到编辑菜单
        final JMenuItem mi_paste=new JMenuItem();    //创建粘贴菜单项
        mi_paste.setText("粘贴");    //设置粘贴菜单项的标题
        mn_edit.add(mi_paste);    //把粘贴菜单项添加到编辑菜单
        final JToolBar toolBar=new JToolBar();    //创建工具栏
        getContentPane().add(toolBar, BorderLayout.NORTH);    //把工具栏放到窗体上方
        final JButton btn_open=new JButton();    //创建工具按钮

        btn_open.setText("  打  开  ");    //设置工具按钮的标题
        toolBar.add(btn_open);    //把工具按钮添加到工具栏上
        final JButton btn_exit=new JButton();    //创建工具按钮

        btn_exit.setText("  退  出  ");    //设置工具按钮的标题
        toolBar.add(btn_exit);    //把工具按钮添加到工具栏上
        final JTabbedPane tabbedPane=new JTabbedPane();    //创建选项卡面板
        getContentPane().add(tabbedPane,BorderLayout.CENTER);    //把选项卡面板放到窗体中央
        final JScrollPane scrollPane1=new JScrollPane();    //创建滚动面板
        //把滚动面板放到选项卡的第一个选项页
        tabbedPane.addTab("文件的属性",null,scrollPane1,null);
        ta_showProperty=new JTextArea();    //创建文本域
        //把文本域添加到滚动面板的视图中
        scrollPane1.setViewportView(ta_showProperty);
        final JScrollPane scrollPane2=new JScrollPane();    //创建滚动面板
        //把滚动面板放到选项卡的第二个选项页
        tabbedPane.addTab("文件的内容",null,scrollPane2,null);
        ta_showText=new JTextArea();    //创建文本域
        //把文本域添加到滚动面板的视图中
        scrollPane1.setViewportView(ta_showText);


    }

    public static void openTextFile(String info) throws BadLocationException {
        ta_showText.append(info + "\n");    //把读到的信息追加到文本域中
        ta_showText.setCaretPosition(ta_showText.getDocument().getLength());

    }

    public static class TrustAnyTrustManager implements X509TrustManager {

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    public static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
