package com.getcodeweb.demo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.util.HttpClientUtil;
import com.getcodeweb.demo.util.TrustSSL;
import com.sun.jna.platform.win32.BaseTSD;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;

import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;
import sun.net.www.http.HttpClient;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainTest {
    public static class TrustAnyTrustManager implements X509TrustManager {

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    public static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }


    public static void main(String[] args) throws Exception {
        System.out.println(new Date());
//        Document document = Jsoup.connect("https://pub.icaile.com/sd11x5/").timeout(9999).proxy("125.118.71.35",808).get();
//        System.out.println(document.html());  http://167.179.75.153:5010/get_all/
 //       Proxy proxys = new Proxy(java.net.Proxy.Type.HTTP,new InetSocketAddress("221.5.80.66", 3128));
        //URL console = new URL("https://liveinfo.yangshipin.cn/?cmd=2&cnlid=2000204603&pla=0&stream=2&system=0&appVer=3.0.6&encryptVer=8.1&qq=0&device=PC&guid=kdrwcvmr_rv1ihjuacza&defn=&host=yangshipin.cn&livepid=600001811&logintype=1&vip_status=1&livequeue=1&fntick=1597309112&tm=1597309112&sdtfrom=4330701&platform=4330701&cKey=--0123815F19939005DEFEF19998E43218F58535BCEC2533AA7AC9058B87738B3F8130C134664F245A5DB330FD5876EF4CA9F4F89C55FFD56D41928BB494D08C274DA6CC99CFE34ADDB06F02E36C3D53193F3FCE7B1663C819C28BB3C7B81DAC98BB07B7770434C5A937BEB6B0639AA610FFC48633131DA131442EB11EB3BF6BAFBA94E8073FAE496495A7AA7705FC288F11D92936B2A5869ADC4D6A288712D05711&queueStatus=0&uhd_flag=4&flowid=73b141772fef339850722f7d3f3b76c8&sphttps=1&callback=txvlive_videoinfoget_85670108");
        while (true){

        URL console = new URL("http://167.179.75.153:5010/get_all/");
        HttpURLConnection conn = (HttpURLConnection) console.openConnection();
       // FlushOpenCodeEntity flushOpenCodeEntity = new FlushOpenCodeEntity();
        if (conn instanceof HttpsURLConnection)  {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{new TrustSSL.TrustAnyTrustManager()}, new java.security.SecureRandom());
            ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
            ((HttpsURLConnection) conn).setHostnameVerifier(new TrustSSL.TrustAnyHostnameVerifier());
            ((HttpsURLConnection) conn).setReadTimeout(999999);
        }
        conn.connect();
        //System.out.println(conn);
        InputStream inputStream = conn.getInputStream();
        //获取响应
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder stringBuilder =new StringBuilder();
        while ((line = reader.readLine()) != null){
            stringBuilder.append(line);
            //System.out.println(line);
        }

       // System.out.println(stringBuilder);
        List<JSONObject> jsonObject = JSONObject.parseArray(stringBuilder.toString(),JSONObject.class);
        for (JSONObject jsonObject1:jsonObject){
            JSONObject jsonObject2 = JSONObject.parseObject(jsonObject1.toJSONString());
            String []proxy=jsonObject2.get("proxy").toString().split(":");
            if(jsonObject2.get("proxy").toString().equals("49.4.67.31:3128"))continue;

            try {

                long l = System.currentTimeMillis();
                Document document = Jsoup.connect("https://pub.icaile.com/sd11x5/").timeout(10000).proxy(proxy[0], Integer.parseInt(proxy[1])).get();
                System.out.println("可以使用的代理ip："+proxy[0]+":"+proxy[1]+",响应时间："+(System.currentTimeMillis()-l));
                System.out.println(document.text());
            }catch (Exception e){

                //e.printStackTrace();
            }
        }
        reader.close();
        //该干的都干完了,记得把连接断了
        conn.disconnect();

        }
    }

    static WinUser.INPUT input = new WinUser.INPUT(  );
    static void  sendChar(char ch){



        input.type = new WinDef.DWORD( WinUser.INPUT.INPUT_KEYBOARD );
        input.input.setType("ki"); // Because setting INPUT_INPUT_KEYBOARD is not enough: https://groups.google.com/d/msg/jna-users/NDBGwC1VZbU/cjYCQ1CjBwAJ
        input.input.ki.wScan = new WinDef.WORD( 0 );
        input.input.ki.time = new WinDef.DWORD( 0 );
        input.input.ki.dwExtraInfo = new BaseTSD.ULONG_PTR( 0 );
        // Press
        input.input.ki.wVk = new WinDef.WORD( Character.toUpperCase(ch) ); // 0x41
        input.input.ki.dwFlags = new WinDef.DWORD( 0 );  // keydown

        User32.INSTANCE.SendInput( new WinDef.DWORD( 1 ), ( WinUser.INPUT[] ) input.toArray( 1 ), input.size() );

        // Release
        input.input.ki.wVk = new WinDef.WORD( Character.toUpperCase(ch) ); // 0x41
        input.input.ki.dwFlags = new WinDef.DWORD( 2 );  // keyup

        User32.INSTANCE.SendInput( new WinDef.DWORD( 1 ), ( WinUser.INPUT[] ) input.toArray( 1 ), input.size() );

    }
    @Test
    public void getBaiHe() throws InterruptedException {
        String a ="200605010";
        System.out.println(a.substring(6));
    }

    @Test
    public void unicodeTest() throws Exception{
//        Map<String,String> requestHeard =new HashMap<>();
//        requestHeard.put("Content-Type","application/xml");
//        requestHeard.put("Accept","application/json, text/javascript, */*; q=0.01");
//        requestHeard.put("Accept-Encoding","gzip, deflate");
//        requestHeard.put("Accept-Language","zh-CN,zh;q=0.9,en;q=0.8");
//        requestHeard.put("Connection","keep-alive");
//        requestHeard.put("Cookie","UniqueID=uquwpxyenxJmWdgN1591602756136; Sites=_21; _ga=GA1.3.2016803185.1587313009; HMF_CI=aa8cf131fa8d7f10f18f2be4039e5436ca9511fc6b7275df54a361b125583fea8c; _Jo0OQK=51B6C85076D324FF0275B83B9546E602A98C4EA164AA792B4C2E39F88CE3EE6FE92FFB69274416886053E4954DAEF7A48CCB69CB8961FC3499A0F3AA76173828D11F1B3C19C5B2FC5F8E6E66EDA7420CD4BE6E66EDA7420CD4B8510C48E9771AFBB1832EF0D0AF58593GJ1Z1bA==; _gid=GA1.3.886933374.1591602757; 21_vq=5; HYB_SH=a283d08ed7c96b64e95c1bc9f9145acb6f; HMY_JC=55fa895885f2de1212feabdb02ba6f311d97ea3838065cba961ef87cd0957f6157,1958; HBB_HC=ef9d3e5e3be29aa67fcc4c00ec83a6ca7e369595f5db9d611030e8eaef5881e4e92896e02037a0b1108bd7ef8603f10fec");
//        requestHeard.put("Host","www.cwl.gov.cn");
//        requestHeard.put("Referer","http://www.cwl.gov.cn/kjxx/fc3d/");
//        requestHeard.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36");
//        requestHeard.put("X-Requested-With","XMLHttpRequest");
//        JSONObject jsonObject = HttpClientUtil.httpGet1680380("https://api.api861861.com/pks/getPksHistoryList.do?lotCode=10001");
//        List<Object> objects=(List<Object>)jsonObject.get("result");
//        System.out.println("列表长度"+objects.size());
//        for(Object o:objects){
//            JSONObject jsonObject1 = JSONObject.parseObject(o.toString());
//            System.out.println(o);
//
//        }
        URL console = new URL("https://api.api861861.com/pks/getPksHistoryList.do?lotCode=10001");
        HttpURLConnection conn = (HttpURLConnection) console.openConnection();
        FlushOpenCodeEntity flushOpenCodeEntity = new FlushOpenCodeEntity();
        if (conn instanceof HttpsURLConnection)  {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{new TrustSSL.TrustAnyTrustManager()}, new java.security.SecureRandom());
            ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
            ((HttpsURLConnection) conn).setHostnameVerifier(new TrustSSL.TrustAnyHostnameVerifier());
        }
        conn.connect();
        //System.out.println(conn);
        InputStream inputStream = conn.getInputStream();
        //获取响应
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder stringBuilder =new StringBuilder();
        while ((line = reader.readLine()) != null){
            stringBuilder.append(line);
        }
        List<Object> list =new ArrayList<>();
        JSONObject jsonObject = JSON.parseObject(stringBuilder.toString());
        JSONObject jsonObject1 = JSON.parseObject(jsonObject.get("result").toString());
        list=(List<Object>)jsonObject1.get("data");
        for (Object o:list){
            JSONObject jsonObject2 = JSONObject.parseObject(o.toString());
            Object preDrawIssue = jsonObject2.get("preDrawIssue");
            Object preDrawTime = jsonObject2.get("preDrawTime");
            Object preDrawCode = jsonObject2.get("preDrawCode");
            System.out.println("期号："+preDrawIssue+",开奖时间："+preDrawTime+"，开奖号码："+preDrawCode);
        }
//        URL console = new URL("http://www.cwl.gov.cn/cwl_admin/kjxx/findDrawNotice?name=3d&issueCount=5");
//        HttpURLConnection conn = (HttpURLConnection) console.openConnection();
//        FlushOpenCodeEntity flushOpenCodeEntity = new FlushOpenCodeEntity();
//        if (conn instanceof HttpsURLConnection)  {
//            conn.setRequestProperty("Content-Type","application/xml");
//            conn.setRequestProperty("Accept","application/json, text/javascript, */*; q=0.01");
//            conn.setRequestProperty("Accept-Encoding","gzip, deflate");
//            conn.setRequestProperty("Accept-Language","zh-CN,zh;q=0.9,en;q=0.8");
//            conn.setRequestProperty("Connection","keep-alive");
//            conn.setRequestProperty("Cookie","UniqueID=uquwpxyenxJmWdgN1591602756136; Sites=_21; _ga=GA1.3.2016803185.1587313009; HMF_CI=aa8cf131fa8d7f10f18f2be4039e5436ca9511fc6b7275df54a361b125583fea8c; _Jo0OQK=51B6C85076D324FF0275B83B9546E602A98C4EA164AA792B4C2E39F88CE3EE6FE92FFB69274416886053E4954DAEF7A48CCB69CB8961FC3499A0F3AA76173828D11F1B3C19C5B2FC5F8E6E66EDA7420CD4BE6E66EDA7420CD4B8510C48E9771AFBB1832EF0D0AF58593GJ1Z1bA==; _gid=GA1.3.886933374.1591602757; 21_vq=5; HYB_SH=a283d08ed7c96b64e95c1bc9f9145acb6f; HMY_JC=55fa895885f2de1212feabdb02ba6f311d97ea3838065cba961ef87cd0957f6157,1958; HBB_HC=ef9d3e5e3be29aa67fcc4c00ec83a6ca7e369595f5db9d611030e8eaef5881e4e92896e02037a0b1108bd7ef8603f10fec");
//            conn.setRequestProperty("Host","www.cwl.gov.cn");
//            conn.setRequestProperty("Referer","http://www.cwl.gov.cn/kjxx/fc3d/");
//            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36");
//            conn.setRequestProperty("X-Requested-With","XMLHttpRequest");
//            SSLContext sc = SSLContext.getInstance("SSL");
//            sc.init(null, new TrustManager[]{new TrustSSL.TrustAnyTrustManager()}, new java.security.SecureRandom());
//            ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
//            ((HttpsURLConnection) conn).setHostnameVerifier(new TrustSSL.TrustAnyHostnameVerifier());
//        }
//        conn.connect();
//        //System.out.println(conn);
//        InputStream inputStream = conn.getInputStream();
//        //获取响应
//        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//        String line;
//        StringBuilder stringBuilder =new StringBuilder();
//        while ((line = reader.readLine()) != null){
//            stringBuilder.append(line);
//            //System.out.println(line);
//        }
//        System.out.println(stringBuilder.toString());
//
//        reader.close();
//        //该干的都干完了,记得把连接断了
//        conn.disconnect();


    }


}

