package com.getcodeweb.demo.ai;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.UnsupportedEncodingException;

public class Main {
    public static void main(String[] args) throws TesseractException, UnsupportedEncodingException {
        ITesseract instance = new Tesseract();
        // 指定训练数据集合的路径
        instance.setDatapath("D:\\Tesseract-OCR\\tessdata");
        // 指定为中文识别
        instance.setLanguage("chi_sim");

        // 指定识别图片
        File imgDir = new File("C:\\Users\\GUO\\Desktop\\微信截图_20211211041129.png");
        ImageIO.scanForPlugins();
        long startTime = System.currentTimeMillis();
        String ocrResult = instance.doOCR(imgDir);

        // 输出识别结果
        System.out.println("OCR Result: \n" + ocrResult + "\n 耗时：" + (System.currentTimeMillis() - startTime) + "ms");
    }
}
