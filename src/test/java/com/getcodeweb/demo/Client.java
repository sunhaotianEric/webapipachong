package com.getcodeweb.demo;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

public class Client {

    public static void main(String[] args) throws IOException,InterruptedException {

       // File filename = new File("./betProperty.txt");
        File filename1 = new File("C:\\Users\\ADMIN\\Desktop\\betProperty1.txt");
        File filename2 = new File("C:\\Users\\ADMIN\\Desktop\\betProperty2.txt");
        List<File>  fileList = new ArrayList<>();
        //fileList.add(filename);
        fileList.add(filename1);
        fileList.add(filename2);

        for (int i =0;i<fileList.size();i++){
            Timer timer = new Timer();
            File file= fileList.get(i);

            BufferedReader reader= null;
            String tempString = null;
            StringBuilder stringBuilder = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
            Map<String,String > map= new HashMap<>();
            while ((tempString = reader.readLine()) != null) {
                if (tempString.startsWith("\"betNum\"")){
                    String[] split = tempString.split(":");
                    map.put("betNum",split[1]);
                    continue;
                }
                stringBuilder.append(tempString);
            }

            JSONObject jsonObject = JSONObject.parseObject(JSON.toJSON(stringBuilder).toString());
            System.out.println("用户id："+jsonObject.get("sessionId")+"配置参数："+JSON.toJSON(stringBuilder).toString());
            reader.close();

            timer.schedule(new TimerTask() {
                @SneakyThrows
                @Override
                public void run() {
                    System.err.println(jsonObject.get("sessionId") + "正在执行,投注彩种："+jsonObject.get("lotrry")+"\n，投注号码："+map.get("betNum")+"期号："+jsonObject.get("startCode"));
                    int num=Integer.parseInt(jsonObject.getString("startCode"));

                    PrintWriter out = null;
                    BufferedReader in = null;
                    String result = "";
                    String param = "";
                    Map<String,String> paramMap=new HashMap<>();
                    paramMap.put("command","BET");
                    paramMap.put("sessionId",jsonObject.getString("sessionId"));
                    paramMap.put("oddsAdapt","true");
                    paramMap.put("bets",map.get("betNum"));
                    paramMap.put("gameType",jsonObject.getString("lotrry"));
                    paramMap.put("gameNo",num+"");
                    paramMap.put("timestamps",new Date().getTime()+"");
                    paramMap.put("hasPlayerInfo","true");
                    Iterator<String> it = paramMap.keySet().iterator();
                    while (it.hasNext()) {
                        String key = it.next();
                        param += key + "=" + paramMap.get(key) + "&";
                    }
                    URL console = new URL("https://ssc.yykxg1.com/lotteryweb/WebClientAgent?sessionId="+jsonObject.get("sessionId"));
                    HttpURLConnection conn = (HttpURLConnection) console.openConnection();

                    if (conn instanceof HttpsURLConnection)  {
                        SSLContext sc = SSLContext.getInstance("SSL");
                        sc.init(null, new TrustManager[]{new Client.TrustAnyTrustManager()}, new java.security.SecureRandom());
                        ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
                        ((HttpsURLConnection) conn).setHostnameVerifier(new Client.TrustAnyHostnameVerifier());
                        conn.setReadTimeout(999999);
                        conn.setDoOutput(true);
                        conn.setDoInput(true);
                        conn.setRequestProperty(":authority","ssc.yykxg1.com");
                        conn.setRequestProperty(":method","POST");
                        conn.setRequestProperty("::path","/lotteryweb/WebClientAgent?sessionId="+jsonObject.getString("sessionId"));
                        conn.setRequestProperty(":scheme","https");
                        conn.setRequestProperty("accept","*/*");
                        conn.setRequestProperty("accept-encoding","gzip, deflate, br");
                        conn.setRequestProperty("accept-language","zh-CN,zh;q=0.9,en;q=0.8");
                        conn.setRequestProperty("content-length",jsonObject.getString("content-length"));
                        conn.setRequestProperty("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                        conn.setRequestProperty("cookie",jsonObject.getString("cookie"));
                        conn.setRequestProperty("origin","https://ssc.yykxg1.com");
                        conn.setRequestProperty("referer",jsonObject.getString("referer"));
                        conn.setRequestProperty("sec-fetch-dest","empty");
                        conn.setRequestProperty("sec-fetch-mode","cors");
                        conn.setRequestProperty("sec-fetch-site","same-origin");
                        conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
                        conn.setRequestProperty("x-requested-with","XMLHttpRequest");
                    }
                    out = new PrintWriter(conn.getOutputStream());
                    out.print(param);
                    // flush输出流的缓冲
                    out.flush();
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"ISO-8859-1"));
                    String line;
                    while ((line = in.readLine()) != null) {
                        result += line;
                    }
                   if (result.getBytes()[0]==31){
                        System.err.println("投注失败！请检查参数");
                       timer.cancel();
                        return;
                    }
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
                    System.err.println(jsonObject.get("sessionId")+"投注成功,投注号码:"+map.get("betNum")+"ok，期号:"+num+",投注时间:"+dateFormat.format(new Date()));
                    num++;
                    jsonObject.put("startCode",num+"");


                     System.out.println(jsonObject.get("sessionId")+"执行结束，下一期投注期号："+jsonObject.get("startCode"));

                }
            },0,Long.parseLong(jsonObject.getString("openCodeTime")));
            Thread.sleep(5000);
        }

//        while (true){
//
//        }
//
        }

    public static class TrustAnyTrustManager implements X509TrustManager {

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    public static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
