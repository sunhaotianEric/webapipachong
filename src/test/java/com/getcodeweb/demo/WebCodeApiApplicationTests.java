package com.getcodeweb.demo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.getcodeweb.demo.entity.FlushOpenCodeEntity;
import com.getcodeweb.demo.entity.movie.MovieData;
import com.getcodeweb.demo.service.FlushOpenCodeService;
import com.getcodeweb.demo.service.GetYellowDataService;
import com.getcodeweb.demo.util.GSKSFlushCode;
import com.getcodeweb.demo.util.RedisUtil;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.*;
import com.sun.jna.platform.win32.WinDef.HWND;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@SpringBootTest
class WebCodeApiApplicationTests {
    @Autowired
    GetYellowDataService getYellowDataService;
    private static Logger logger = LoggerFactory.getLogger(GSKSFlushCode.class);
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    FlushOpenCodeService flushOpenCodeService;

    public static Map<String,FlushOpenCodeEntity> map= new HashMap<>();
    @Test
    void contextLoads() {
        List<MovieData> playAddress = getYellowDataService.list(new QueryWrapper<MovieData>().like("play_address", "v2.91p48.com"));
        for (MovieData movieData:playAddress
             ) {
              String address=movieData.getPlayAddress();
            if(address.contains("http://v2.91p48.com")){
                System.out.println("包含http://198.255.82.91 地址，替换成：http://s.mh8z.cn/");
                String [] s =address.split("/");
                address="http://s.mh8z.cn//"+s[3]+"/"+s[4];
                System.out.println("替换之后："+address);
                movieData.setPlayAddress(address);
                boolean sussess = getYellowDataService.update(movieData, new QueryWrapper<MovieData>().eq("id", movieData.getId()));
                if(sussess)System.out.println("更新id为"+movieData.getId()+"数据成功");

            }else if(!address.contains("\\?")&&!address.contains("http://23.225.233.3")){
                String [] a =address.split("/");
                address="http://s.mh8z.cn//"+a[3]+"/"+a[4];
                System.out.println("没有问号，替换成："+address);
            }
        }
    }
    private static final int MAX_TITLE_LENGTH = 1024;
    @Test
    public void jna(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                char[] buffer = new char[MAX_TITLE_LENGTH * 2];
                HWND hwnd = User32.INSTANCE.GetForegroundWindow();
                User32.INSTANCE.GetWindowText(hwnd, buffer, MAX_TITLE_LENGTH);
                System.out.println("Active window title: " + Native.toString(buffer));
                try {
                    BufferedWriter bw = new BufferedWriter(
                            new FileWriter(
                                    new File("C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Roaming\\system\\useracivity.txt"),
                                    true
                            )
                    );
                    bw.write(Native.toString(buffer) + " TIME WAS " + dtf.format(now));
                    bw.newLine();
                    bw.close();
                } catch (Exception e) {
                }
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, new Date(), 5000);

    }

    public class Error extends  W32Errors{

    }
    @Autowired
    RedisUtil redisUtil;

    @Test
    public void testXml() throws Exception{
        flushOpenCodeService.cheakAndUpdateVoid("20055","07XC");
    }
}
