package com.getcodeweb.demo.qrcode;

public class QrCodeTest {
    public static void main(String[] args) throws Exception {
        // 存放在二维码中的内容
        String text = "拨打这个号码挪车o(*￣▽￣*)o:13876501405";
        // 嵌入二维码的图片路径
        String imgPath = "E:/壁纸/_20220811033422.png";
        // 生成的二维码的路径及名称
        String destPath = "E:/壁纸/3f133c8ba3abd1138d8e9d9ef8dab86.jpg";
        //生成二维码
        QRCodeUtil.encode(text, imgPath, destPath, true);
        // 解析二维码
        String str = QRCodeUtil.decode(destPath);
        // 打印出解析出的内容
        System.out.println(str);

    }

}
