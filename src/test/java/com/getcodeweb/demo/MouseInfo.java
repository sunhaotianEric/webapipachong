package com.getcodeweb.demo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.Timer;

public class MouseInfo extends JFrame {
    public static JPanel contentPanel = new JPanel();
    public static List<String> infos= new ArrayList<>();
    public static JTextArea ta_showText;
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        MouseInfo info_frame = new MouseInfo();
        info_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        info_frame.setVisible(true);
        info_frame.setAlwaysOnTop(true);
        try {
            File filename = new File("C:\\Users\\ADMIN\\TestCmd\\betProperty.txt");
            File filename1 = new File("C:\\Users\\ADMIN\\TestCmd\\betProperty1.txt");
            File filename2 = new File("C:\\Users\\ADMIN\\TestCmd\\betProperty2.txt");
            List<File> fileList = new ArrayList<>();
            fileList.add(filename);
            fileList.add(filename1);
            fileList.add(filename2);


            for (int i =0;i<fileList.size();i++){
                Timer timer = new Timer();
                File file= fileList.get(i);

                BufferedReader reader= null;
                String tempString = null;
                StringBuilder stringBuilder = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
                Map<String,String > map= new HashMap<>();
                while ((tempString = reader.readLine()) != null) {
                    if (tempString.startsWith("\"betNum\"")){
                        String[] split = tempString.split(":");
                        map.put("betNum",split[1]);
                        continue;
                    }
                    stringBuilder.append(tempString);
                }

                JSONObject jsonObject = JSONObject.parseObject(JSON.toJSON(stringBuilder).toString());
                printInfo("用户id："+jsonObject.get("sessionId")+"配置参数：\n"+JSON.toJSON(stringBuilder).toString());
                reader.close();

                timer.schedule(new TimerTask() {
                    @SneakyThrows
                    @Override
                    public void run() {
                        printInfo(jsonObject.get("sessionId") + "正在执行,投注彩种："+jsonObject.get("lotrry")+"\n，投注号码："+map.get("betNum")+"期号："+jsonObject.get("startCode"));
                        int num=Integer.parseInt(jsonObject.getString("startCode"));

                        PrintWriter out = null;
                        BufferedReader in = null;
                        String result = "";
                        String param = "";
                        Map<String,String> paramMap=new HashMap<>();
                        paramMap.put("command","BET");
                        paramMap.put("sessionId",jsonObject.getString("sessionId"));
                        paramMap.put("oddsAdapt","true");
                        paramMap.put("bets",map.get("betNum"));
                        paramMap.put("gameType",jsonObject.getString("lotrry"));
                        paramMap.put("gameNo",num+"");
                        paramMap.put("timestamps",new Date().getTime()+"");
                        paramMap.put("hasPlayerInfo","true");
                        Iterator<String> it = paramMap.keySet().iterator();
                        while (it.hasNext()) {
                            String key = it.next();
                            param += key + "=" + paramMap.get(key) + "&";
                        }
                        URL console = new URL("https://ssc.yykxg1.com/lotteryweb/WebClientAgent?sessionId="+jsonObject.get("sessionId"));
                        HttpURLConnection conn = (HttpURLConnection) console.openConnection();
                        if (conn instanceof HttpsURLConnection)  {
                            SSLContext sc = SSLContext.getInstance("SSL");
                            sc.init(null, new TrustManager[]{new Client.TrustAnyTrustManager()}, new java.security.SecureRandom());
                            ((HttpsURLConnection) conn).setSSLSocketFactory(sc.getSocketFactory());
                            ((HttpsURLConnection) conn).setHostnameVerifier(new Client.TrustAnyHostnameVerifier());
                            conn.setReadTimeout(999999);
                            conn.setDoOutput(true);
                            conn.setDoInput(true);
                            conn.setRequestProperty(":authority","ssc.yykxg1.com");
                            conn.setRequestProperty(":method","POST");
                            conn.setRequestProperty("::path","/lotteryweb/WebClientAgent?sessionId="+jsonObject.getString("sessionId"));
                            conn.setRequestProperty(":scheme","https");
                            conn.setRequestProperty("accept","*/*");
                            conn.setRequestProperty("accept-encoding","gzip, deflate, br");
                            conn.setRequestProperty("accept-language","zh-CN,zh;q=0.9,en;q=0.8");
                            conn.setRequestProperty("content-length",jsonObject.getString("content-length"));
                            conn.setRequestProperty("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                            conn.setRequestProperty("cookie",jsonObject.getString("cookie"));
                            conn.setRequestProperty("origin","https://ssc.yykxg1.com");
                            conn.setRequestProperty("referer",jsonObject.getString("referer"));
                            conn.setRequestProperty("sec-fetch-dest","empty");
                            conn.setRequestProperty("sec-fetch-mode","cors");
                            conn.setRequestProperty("sec-fetch-site","same-origin");
                            conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
                            conn.setRequestProperty("x-requested-with","XMLHttpRequest");
                        }
                        out = new PrintWriter(conn.getOutputStream());
                        out.print(param);
                        // flush输出流的缓冲
                        out.flush();
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"ISO-8859-1"));
                        String line;
                        while ((line = in.readLine()) != null) {
                            result += line;
                        }
                        printInfo(jsonObject.get("sessionId")+"投注成功,\n投注号码:"+map.get("betNum")+"ok，期号:"+num+",投注时间:"+new Date());
                        num++;
                        jsonObject.put("startCode",num+"");

                        printInfo(jsonObject.get("sessionId")+"执行结束，\n下一期投注期号："+jsonObject.get("startCode"));

                    }
                },0,Long.parseLong(jsonObject.getString("openCodeTime")));
                Thread.sleep(5000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog.
     */
    public MouseInfo() {
        setTitle("在线发包工具");
        setBounds(100, 100, 1250, 650);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(0, 100, 100, 100));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        ta_showText=new JTextArea();    //创建文本域
        final JScrollPane scrollPane1=new JScrollPane();
        final JTabbedPane tabbedPane=new JTabbedPane();
        scrollPane1.setViewportView(ta_showText);
        contentPanel.setLayout(null);
        tabbedPane.addTab("文件的内容",null,scrollPane1,null);



    }
    static  int y=0;
    static  int height=17;
    public static void printInfo(String info){
        ta_showText.append(info + "\n");    //把读到的信息追加到文本域中

    }

}
