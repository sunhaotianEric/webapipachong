package com.getcodeweb.demo;

import com.alibaba.fastjson.JSONObject;
import com.getcodeweb.demo.util.HttpClientUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.io.IOException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

public class RSAEncrypt {
    private static RequestConfig requestConfig;

    static {
        // 设置请求和传输超时时间
        requestConfig = RequestConfig.custom().setSocketTimeout(2000).setConnectTimeout(5 * 2000).setSocketTimeout(3 * 1000).build();
    }

    /**
     * post请求传输json参数
     *
     * @param url url地址
     * @return
     */
    public static JSONObject httpPost(String url, JSONObject jsonParam) {
        // post请求返回结果
        CloseableHttpClient httpClient = HttpClients.createDefault();
        JSONObject jsonResult = null;
        HttpPost httpPost = new HttpPost(url);
        // 设置请求和传输超时时间
        httpPost.setConfig(requestConfig);
        httpPost.setHeader("token","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjE3MTk5OTQxMTU0IiwiZXhwIjoxNTk3NDg3NzY5fQ.T1JMiSW6voEu904LVi0XBI81HhAvakBIS2uyMq6VmKk");
        httpPost.setHeader("Accept","application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding","zip, deflate");
        httpPost.setHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
        httpPost.setHeader("withCredentials","true");
        httpPost.setHeader("Origin","http://www.hhplan02.com");
        httpPost.setHeader("Referer","http://www.hhplan02.com/");
        httpPost.setHeader("Connection","keep-alive");

        try {
            if (null != jsonParam) {
                // 解决中文乱码问题
                StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json");
                httpPost.setEntity(entity);
            }
            CloseableHttpResponse result = httpClient.execute(httpPost);
            // 请求发送成功，并得到响应
            if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String str = "";
                try {
                    // 读取服务器返回过来的json字符串数据
                    str = EntityUtils.toString(result.getEntity(), "utf-8");
                    // 把json字符串转换成json对象
                    jsonResult = JSONObject.parseObject(str);
                } catch (Exception e) {
                    //logger.error("post请求提交失败:" + url, e);
                }
            }
        } catch (IOException e) {
            //logger.error("post请求提交失败:" + url, e);
        } finally {
            httpPost.releaseConnection();
        }
        return jsonResult;
    }

    public static void main(String[] args) {


        JSONObject jsonObject = new JSONObject();
                                                        jsonObject.put("headUrl","https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597446620206&di=4ebebfbe53d673b499398f273276972c&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%3D580%2Fsign%3D9f2752ac007b02080cc93fe952d9f25f%2F2e837c1ed21b0ef4eaf4e257d7c451da81cb3e58.jpg");
        jsonObject.put("nickName","哈哈哈");
        jsonObject.put("signature","a");
        jsonObject.put("uId","286");


        System.out.println(httpPost("http://47.104.109.132/user/updateUser", jsonObject));
    }

}
